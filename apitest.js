const fs = require('fs')
const { promisify } = require('util')
const axios = require('axios');
const readFileAsync = promisify(fs.readFile)


const run = async () => {
    const res = await readFileAsync('./sample.json')
    const data = Buffer.from(res);
    doPostRequest(data)
  }

  async function doPostRequest(payload) {
    console.log(payload)

    let res = await axios.post('https://reqres.in/api/users', payload);
    let data = res.data;
    console.log(`Status: ${res.status}`);
    console.log(data);
}
run()
